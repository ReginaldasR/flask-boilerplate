#!flask/bin/python
from app import app
from config import DEBUG

app.run(debug=DEBUG)